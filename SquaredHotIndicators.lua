SHI = {}

-- Make Squared table ref local for performance
local Squared = Squared

-- Make utility functions local for performance
local pairs = pairs
local tinsert = table.insert
local tremove = table.remove

local ShowCastBarHook
local HideCastBarHook
local timePassed = 0
local renderData = {}
local castCache = {}
local playerName = L""
local unitCache = {}
local updateCache = {}
local hotCache = {}
local iconTable = {}
local spellTable = {
	 -- Shaman
	 [1901] = {colorType = 1, duration = 15}, -- 'Ey, Quit Bleedin'
	 [1898] = {colorType = 2, duration = 5},	-- Gork'll Fix It
	 [1926] = {colorType = 3, duration = 9},	-- Do Sumfin Useful
	 
	 -- Zealot
	 [8558] = {colorType = 1, duration = 15}, -- Tzeentch's Cordial
	 [8549] = {colorType = 2, duration = 5},	-- Dark Medicine
	 [8557] = {colorType = 3, duration = 9},	-- Leaping Alteration
	 
	 -- Disciple
	 [9550] = {colorType = 1, duration = 15}, -- Soul Infusion
	 [9548] = {colorType = 2, duration = 5},	-- Restore Essence
	 [9573] = {colorType = 3, duration = 15, group = true}, -- Khaine's Vigor
	 
	 -- Archmage
	 [9238] = {colorType = 1, duration = 15}, -- Lambent Aura
	 [9236] = {colorType = 2, duration = 5},	-- Healing Energy
	 
	 -- Warrior Priest
	 [8241] = {colorType = 1, duration = 15}, -- Healing Hand
	 [8238] = {colorType = 2, duration = 5},	-- Divine Aid
	 [8265] = {colorType = 3, duration = 15, group = true}, -- Pious Restoration

	 -- Rune Priest
	 [1590] = {colorType = 1, duration = 15}, -- Rune of Regeneration
	 [1599] = {colorType = 2, duration = 5},	-- Rune of Mending
	 [1601] = {colorType = 3, duration = 9},	-- Rune of Serenity
	}

function SHI.OnInitialize()	
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_EFFECTS_UPDATED, "SHI.EffectsUpdated")
	RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "SHI.EffectsUpdated")
	RegisterEventHandler(SystemData.Events.GROUP_EFFECTS_UPDATED, "SHI.EffectsUpdated")
	
	RegisterEventHandler(SystemData.Events.PLAYER_NEW_ABILITY_LEARNED, "SHI.BuildIconTable")
	RegisterEventHandler(SystemData.Events.LOADING_END, "SHI.BuildIconTable")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "SHI.BuildIconTable")
	
	Squared.RegisterEventHandler("cleargroups", SHI.ClearGroupHandler)
	Squared.RegisterEventHandler("setname", SHI.SetNameHandler)
	
	ShowCastBarHook = LayerTimerWindow.ShowCastBar
	LayerTimerWindow.ShowCastBar = SHI.ShowCastBar
	HideCastBarHook = LayerTimerWindow.HideCastBar
	LayerTimerWindow.HideCastBar = SHI.HideCastBar
	
	playerName = (GameData.Player.name):match(L"([^^]+)")
end


function SHI.BuildIconTable()
	-- used to identify buffData of the Hots, as buffId != abilityId (sometimes)
	-- must be called after LOADING_END
	for k,v in pairs(spellTable) do
		local data = GetAbilityData(k)
		if (data.iconNum) then
			iconTable[data.iconNum] = k
		end
	end
	
	-- update Squared settings here, cause we know they are loaded now
	Squared.SetSetting("hot-color1",Squared.GetSetting("hot-color1") or {0,255,0})
	Squared.SetSetting("hot-color2",Squared.GetSetting("hot-color2") or {255,0,0})
	Squared.SetSetting("hot-color3",Squared.GetSetting("hot-color3") or {255,255,0})
	Squared.SetSetting("hot-color4",Squared.GetSetting("hot-color4") or {255,255,255})
	
	-- also, register our settings panel with squared
	SHI.CreateConfigPanel()
end

-- Castbar Hooks
function SHI.ShowCastBar(...)
	local abilityId, _, desiredCastTime = ...
	ShowCastBarHook(...)
	
	if (spellTable[abilityId]) then
		-- player is casting a hot?
		
		local targetName = TargetInfo:UnitName("selffriendlytarget"):match(L"([^^]+)")
		if not targetName then
			targetName = playerName
		end
		
		if (desiredCastTime == 0) then
			SHI.AddHot(targetName, abilityId, spellTable[abilityId].duration, -1)
			castCache = {}
		else
			castCache = {target = targetName, ability = abilityId}
		end
		
	end
end

function SHI.HideCastBar(...)
	local isCancel = ...
	HideCastBarHook(...)
	
	if (castCache.ability) then
		-- if a hot was casted, apply it to it's original target now
			if (isCancel) then
			castCache = {}
		else
			SHI.AddHot(castCache.target, castCache.ability, spellTable[castCache.ability].duration, -1)
		end
	end
end

function SHI.AddHot(target, ability, duration, index)
	if (not unitCache[target]) then
		-- not interested in
		 return
	end
	
	-- ignore group hots from cast events for now
	if (spellTable[ability].group and index == -1) then
		return
	end

	-- if the hot didn't exist on the target, call an update
	if (not hotCache[target][ability]) then
		SHI.UpdateUnit(target)
	end
	
	hotCache[target][ability] = {}
	hotCache[target][ability].duration = duration
	hotCache[target][ability].index = index
end

function SHI.RemoveHot(target, ability)
	if (not unitCache[target]) then
		-- not interested in
		 return
	end

	if (hotCache[target][ability]) then
		hotCache[target][ability] = nil
		SHI.UpdateUnit(target)
	end
end

function SHI.ClearHots(target)
	if (not unitCache[target]) then
		-- not interested in
		 return
	end

	hotCache[target] = {}
	SHI.UpdateUnit(target);
end

function SHI.UpdateUnit(name)
	for k,v in pairs(unitCache) do
		if (v == name) then
			return
		end
	end
	
	if (unitCache[name]) then
		table.insert(updateCache, name)
	end
end

function SHI.EffectsUpdated(updateType, effects, isFull)
	-- Since PLAYER_EFFECTS_UPDATED doesn't bother to specify the updateType, swap the arguments around
	if type(updateType) == "table" then
			effects, isFull, updateType = updateType, effects, GameData.BuffTargetType.SELF
	end
	
	-- Ignore silly update events.
	if not updateType then return end
	
	local pName = L""
	
	-- What type of update are we dealing with?
	if updateType >= GameData.BuffTargetType.GROUP_MEMBER_START and
		 updateType <= GameData.BuffTargetType.GROUP_MEMBER_END then
			-- This is a group member update
			local groupIndex = updateType - GameData.BuffTargetType.GROUP_MEMBER_START + 1
			pName = (GroupWindow.groupData[groupIndex].name):match(L"([^^]+)")
	elseif updateType == GameData.BuffTargetType.SELF then
			-- This is a player update
			pName = playerName
	elseif updateType == GameData.BuffTargetType.TARGET_FRIENDLY then
			-- This is a friendly target update
			pName = (TargetInfo:UnitName("selffriendlytarget")):match(L"([^^]+)")
			if not pName then return end -- If we don't have a friendly target, there's nothing to update
	else
			return -- (Ignore hostile target or other updates, we don't care.)
	end

	-- If this unit isn't part of our Squared display, we don't care.
	if not unitCache[pName] then return end

	-- just kill all previous data for that unit in case of a full buff update
	if (isFull and target) then
		SHI.ClearHots(pName)
	end

	-- iterate through the effects
	for k,v in pairs(effects) do
		if (v.effectIndex) then
			if (v.castByPlayer and iconTable[v.iconNum]) then
				-- this one is our hot
				SHI.AddHot(pName, iconTable[v.iconNum], v.duration, k)
			end
		else
			-- this effect is empty, thus the buff has vanished from the target
			-- check if it was one of our hots and remove it
			if (hotCache[pName]) then 
				for abilityId, t in pairs(hotCache[pName]) do
					if (t.index == k) then
						SHI.RemoveHot(pName, abilityId)
					end
				end
			end
		end
	end
end

function SHI.OnUpdate(elapsed)
	SHI.UpdateConfigPanel()

	timePassed = timePassed + elapsed
	
	-- update indicators instantly
	if (table.getn(updateCache) > 0) then
		local pName = updateCache[1]
		local unit = unitCache[pName]
		local fName = "SquaredUnit_"..unit.group.."_"..unit.member.."Hot"
		local i = 0
		
		for abilityId, t in pairs(hotCache[pName]) do
			i = i + 1
			WindowSetShowing(fName..i, true)
			WindowSetTintColor(fName..i, unpack(Squared.GetSetting("hot-color"..spellTable[abilityId].colorType)))
		end
		
		for j=i+1, 3 do
			WindowSetShowing(fName..j, false)
		end
		
		table.remove(updateCache, 1)
	end
	
	if (timePassed > 0.1) then
		
		-- remove hots from dead or offline units
		-- there is a small chance that the target isnt really dead - nothing we can do about that
		for name, unit in pairs(unitCache) do
			if (unit.curval == 0 or unit.online == false) then
				SHI.ClearHots(name)
			end
		end
		
		-- update the duration of all hots
		for name, hots in pairs(hotCache) do
			for abilityId, t in pairs(hots) do
				hotCache[name][abilityId].duration = hotCache[name][abilityId].duration - timePassed
				if (hotCache[name][abilityId].duration <= 0) then
					SHI.RemoveHot(name, abilityId)
				end
			end
		end
		timePassed = 0;
	end
end

-- adding the hot indicators to the Squared Frames
function SHI.ClearGroupHandler()
	unitCache = {}
	updateCache = {}
	

	renderData.size = tonumber(Squared.GetSetting("status-size"))/InterfaceCore.GetScale()
	
	local position = Squared.GetSetting("status-hot")
		
	if position == "bottomright" then
		renderData.point = "bottomleft"
		renderData.relpoint = "bottomright"
		renderData.relframe = "BotRight"
	elseif position == "topright" then
		renderData.point = "bottomleft"
		renderData.relpoint = "bottomright"
		renderData.relframe = "TopRight"			
	elseif position == "topleft" then
		renderData.point = "bottomright"
		renderData.relpoint = "bottomleft"
		renderData.relframe = "TopLeft"
	else
		renderData.point = "bottomright"
		renderData.relpoint = "bottomleft"
		renderData.relframe = "BotLeft"	
	end
end

function SHI.SetNameHandler(unit)
	local wname = "SquaredUnit_"..unit.group.."_"..unit.member
	unitCache[unit.name] = unit

	if (not hotCache[unit.name]) then
		hotCache[unit.name] = {}
	end

	SHI.UpdateUnit(unit.name)

	-- hide the default SBI-Thingie
	WindowSetAlpha(wname..renderData.relframe,0)
	WindowSetDimensions(wname..renderData.relframe,0,0)

	if (not DoesWindowExist(wname.."Hot1")) then
		for i=1,3 do
			local fname = wname.."Hot"..i
			if (not DoesWindowExist(fname)) then
				CreateWindowFromTemplate(fname, "EA_FullResizeImage_TintableSolidBackground", wname)
			end
			WindowSetShowing(fname, false)
			WindowClearAnchors(fname)
			WindowSetDimensions(fname, renderData.size, renderData.size)
			WindowSetLayer(fname,Window.Layers.OVERLAY)
			
			if (i == 1) then
				WindowAddAnchor(fname, renderData.point, wname..renderData.relframe, renderData.point, 0, 0)
			else
				WindowAddAnchor(fname, renderData.point, wname.."Hot"..(i-1), renderData.relpoint, 0, 0)
			end
		end
	end
end










------------------------------------------------------------------------------------------------
-------- Config Panel Functions
------------------------------------------------------------------------------------------------





local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel
local panel = {}

local oRed,oGreen,oBlue
oRed,oGreen,oBlue = 255, 255, 255
local oGroup = 0

-- Our panel window
local W

panel.title = L"HoT Coloring"

-- Create the panel
function CreatePanel(self)
		-- Do we somehow have a window still around? If so, we're done, that was easy.
		if W and W.name then return W end
		
		W = LibGUI("Blackframe")
		W:Resize(500, 500)
		
		local e
		
		-- Title label
		e = W("Label")
		e:Resize(480)
		e:AnchorTo(W, "top", "top", 0, 10)
		e:Font("font_clear_medium_bold")
		e:SetText(L"HoT Color Options")
		W.Title = e
		
		-- Color option select label
		e = W("Label")
		e:Resize(480)
		e:AnchorTo(W.Title, "bottom", "top", 0, 40)
		e:SetText(L"Select a hot group to edit:")
		W.LabelSelectCG = e
		
		-- Color option select combobox
		e = W("Combobox")
		e:AnchorTo(W.LabelSelectCG, "bottom", "top", 0, 10)
		e:Add("hot group 1 (15 sec)")
		e:Add("hot group 2 (5 sec)")
		e:Add("hot group 3 (special)")
		e:SelectIndex(1)
		W.EditSelectCG = e
		
		-- Preview label
		e = W("Label")
		e:AnchorTo(W.EditSelectCG, "bottom", "top", 0, 30)
		e:Resize(480)
		e:Font("font_clear_large_bold")
		e:Color(255, 255, 255)
		e:SetText("COLOR PREVIEW")
		W.LabelPreview = e
		
		-- Color sliders
		e = W("Slider")
		e:AnchorTo(W.LabelPreview, "bottom", "top", 0, 40)
		e:SetRange(0,255)
		W.SliderRed = e
		
		e = W("Label")
		e:Resize(100)
		e:Align("rightcenter")
		e:AnchorTo(W.SliderRed, "left", "right", -10, 0)
		e:SetText("Red")
		W.LabelRed = e
		
		e = W("Textbox")
		e:Resize(50)
		e:AnchorTo(W.SliderRed, "right", "left", 10, 0)
		e:SetText(255)
		W.EditRed = e
		
		e = W("Slider")
		e:AnchorTo(W.SliderRed, "bottom", "top", 0, 10)
		e:SetRange(0,255)
		W.SliderGreen = e
		
		e = W("Label")
		e:Resize(100)
		e:Align("rightcenter")
		e:AnchorTo(W.SliderGreen, "left", "right", -10, 0)
		e:SetText("Green")
		W.LabelGreen = e
		
		e = W("Textbox")
		e:Resize(50)
		e:AnchorTo(W.SliderGreen, "right", "left", 10, 0)
		e:SetText(255)
		W.EditGreen = e
		
		e = W("Slider")
		e:AnchorTo(W.SliderGreen, "bottom", "top", 0, 10)
		e:SetRange(0,255)
		W.SliderBlue = e
		
		e = W("Label")
		e:Resize(100)
		e:Align("rightcenter")
		e:AnchorTo(W.SliderBlue, "left", "right", -10, 0)
		e:SetText("Blue")
		W.LabelBlue = e
		
		e = W("Textbox")
		e:Resize(50)
		e:AnchorTo(W.SliderBlue, "right", "left", 10, 0)
		e:SetText(255)
		W.EditBlue = e
		
		-- Reminder note
		e = W("Label")
		e:Resize(480)
		e:AnchorTo(W, "bottom", "bottom", 0, -65)
		e:SetText("Don't forget to hit Apply for each color!")
		W.LabelReminder = e
		
		-- Apply button
		e = W("Button")
		e:Resize(200)
		e:SetText(L"Apply This Color")
		e:AnchorTo(W, "bottomleft", "bottomleft", 40, -20)
		e.OnLButtonUp = function() ApplyPanel() end
		W.ButtonApply = e
		
		-- Revert button
		e = W("Button")
		e:Resize(200)
		e:SetText(L"Revert")
		e:AnchorTo(W, "bottomright", "bottomright", -40, -20)
		e.OnLButtonUp = function() UpdatePanel() end
		W.ButtonRevert = e
		
		return W
end

function ApplyPanel()
		
		Squared.SetSetting("hot-color"..oGroup, {oRed, oGreen, oBlue})
		oGroup = 0
		
		return
end

function UpdatePanel(self, key, value)

	if (oGroup == 0) then return end		
	local rgb = Squared.GetSetting("hot-color"..oGroup)
	oRed, oGreen, oBlue = unpack(rgb)
	
	W.SliderRed:SetValue(oRed)
	W.SliderGreen:SetValue(oGreen)
	W.SliderBlue:SetValue(oBlue)

	W.EditRed:SetText(towstring(oRed))
	W.EditGreen:SetText(towstring(oGreen))
	W.EditBlue:SetText(towstring(oBlue))
	
	W.LabelPreview:Color(oRed, oGreen, oBlue)
	
	return
end

function SHI.CreateConfigPanel()

	if panel.id then return end
	if not SquaredConfigurator then return end

	panel.title = L"HoT Coloring"

	-- Actually add the panel
	panel.create = CreatePanel
	panel.update = UpdatePanel
	panel.id = SquaredConfigurator.AddPanel(panel)

end

function SHI.UpdateConfigPanel()
	if (not W) or (not SquaredConfigurator.ActivePanel == panel.id) then return end

	cGroup = W.EditSelectCG:SelectedIndex()
	if (cGroup ~= oGroup) then
		oGroup = cGroup
		
		local rgb = Squared.GetSetting("hot-color"..oGroup)
		
		if not rgb then return end
		
		oRed, oGreen, oBlue = unpack(rgb)
		
		W.SliderRed:SetValue(oRed)
		W.SliderGreen:SetValue(oGreen)
		W.SliderBlue:SetValue(oBlue)

		W.EditRed:SetText(towstring(oRed))
		W.EditGreen:SetText(towstring(oGreen))
		W.EditBlue:SetText(towstring(oBlue))
		
		W.LabelPreview:Color(oRed, oGreen, oBlue)
	end
	
	local sRed, sGreen, sBlue
	local eRed, eGreen, eBlue
	local updated = false
	local eupdated = false
	
	sRed = math.floor(W.SliderRed:GetValue())
	sGreen = math.floor(W.SliderGreen:GetValue())
	sBlue = math.floor(W.SliderBlue:GetValue())
			
	eRed = tonumber(W.EditRed:GetText()) or math.floor(W.SliderRed:GetValue())
	eGreen = tonumber(W.EditGreen:GetText()) or math.floor(W.SliderGreen:GetValue())
	eBlue = tonumber(W.EditBlue:GetText()) or math.floor(W.SliderBlue:GetValue())
	
	if sRed ~= oRed then
			oRed = sRed
			updated = true
	elseif eRed ~= oRed then
			oRed = eRed
			eupdated = true
	end
	
	if sGreen ~= oGreen then
			oGreen = sGreen
			updated = true
	elseif eGreen ~= oGreen then
			oGreen = eGreen
			eupdated = true
	end
	
	if sBlue ~= oBlue then
			oBlue = sBlue
			updated = true
	elseif eBlue ~= oBlue then
			oBlue = eBlue
			eupdated = true
	end
	
	if not (updated or eupdated) then return end
	
	W.SliderRed:SetValue(oRed)
	W.SliderGreen:SetValue(oGreen)
	W.SliderBlue:SetValue(oBlue)
	
	if not eupdated then
			W.EditRed:SetText(towstring(oRed))
			W.EditGreen:SetText(towstring(oGreen))
			W.EditBlue:SetText(towstring(oBlue))
	end
	
	W.LabelPreview:Color(oRed, oGreen, oBlue)
end