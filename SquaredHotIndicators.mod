<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="SquaredHotIndicators" version="3.7.0" date="09/10/2008" >

		<VersionSettings gameVersion="1.3.1" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<Author name="talvinen" email="" />
		<Description text="Buff/debuff indicator plugin for Squared. Modified to track up to three hots on a single target with different colors." />
				
		<Dependencies>
			<Dependency name="SquaredSettings" />
			<Dependency name="SquaredConfigurator" />
			<Dependency name="EA_CastTimerWindow" />
		</Dependencies>
				
		<Files>
			<File name="SquaredHotIndicators.lua" />
		</Files>
		
		<OnInitialize>
			<CallFunction name="SHI.OnInitialize" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="SHI.OnUpdate" />
		</OnUpdate>
		<OnShutdown/>
		
		<WARInfo>
			<Careers>
				<Career name="DISCIPLE" />
				<Career name="RUNE_PRIEST" />
				<Career name="SHAMAN" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="ZEALOT" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>

		
	</UiMod>
</ModuleFile>
